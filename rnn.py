import warnings
warnings.filterwarnings('ignore')

import tensorflow as tf

class RNN:
    def __init__(self, sequence_length, n_classes, vocab_size, embedding_size,
                cell_type, n_neurons, l2_reg_lambda, trainable=True):
        
        #Placeholders
        self.input_text = tf.placeholder(tf.int32, shape=[None, sequence_length], name='input_text')
        self.input_y = tf.placeholder(tf.float32, shape=[None, n_classes], name='input_y') 
        self.dropout_keep_prob = tf.placeholder(tf.float32, name='dropout_keep_prob')

        l2_loss = tf.constant(0.0)
        text_length = self.get_length(self.input_text)

        #Embedding
        with tf.device('/gpu:0'), tf.name_scope('word-embedding'):
            self.W_text = tf.Variable(tf.random_normal([vocab_size, embedding_size], -1, 1), name='W_text', trainable=trainable)
            # Used to map the input text indices with the W_Text Embedding 
            self.embedded_chars = tf.nn.embedding_lookup(self.W_text, self.input_text)
        
        #RNN
        with tf.name_scope('rnn'):
            cell = self.create_cell(n_neurons, cell_type)
            cell = tf.nn.rnn_cell.DropoutWrapper(cell, output_keep_prob=self.dropout_keep_prob)

            outputs, _ = tf.nn.dynamic_rnn(cell=cell,
                                           inputs=self.embedded_chars,
                                           sequence_length=text_length, 
                                           dtype=tf.float32
                                           )
            self.h_outputs = self.get_relevant(outputs, text_length)
        
        #Scores and Predicition
        with tf.name_scope('output'):
            W = tf.get_variable('W', shape=[n_neurons, n_classes], initializer=tf.contrib.layers.xavier_initializer())
            b = tf.Variable(tf.constant(0.1, shape=[n_classes]), name='b')
            l2_loss += tf.nn.l2_loss(W)
            l2_loss += tf.nn.l2_loss(b)
            
            self.logits = tf.nn.xw_plus_b(self.h_outputs, W, b, name='logits')
            self.predictions = tf.argmax(self.logits, 1, name='predictions')

        #Cross-Entropy Loss
        with tf.name_scope('loss'):
            losses = tf.nn.softmax_cross_entropy_with_logits_v2(logits=self.logits, labels=self.input_y)
            self.loss = tf.reduce_mean(losses) + (l2_reg_lambda*l2_loss)

        #Accuracy
        correct = tf.equal(self.predictions, tf.argmax(self.input_y, axis=1))
        self.accuracy = tf.reduce_mean(tf.cast(correct, tf.float32), name='accuracy')

    
    @staticmethod
    def get_length(seq):
        relevant = tf.sign(tf.abs(seq))
        length = tf.reduce_sum(relevant, 1)
        length = tf.cast(length, tf.int32)
        return length
    
    @staticmethod
    def create_cell(n_neurons, cell_type):
        if cell_type == 'vanilla':
            return tf.nn.rnn_cell.BasicRNNCell(n_neurons)
        elif cell_type == 'lstm':
            return tf.nn.rnn_cell.BasicLSTMCell(n_neurons)
        elif cell_type == 'lstm':
            return tf.nn.rnn_cell.GRUCell(n_neurons)
        else:
            print("Incorrect Cell Type")
            return None
        
    @staticmethod
    def get_relevant(seq, length):
        batch_size = tf.shape(seq)[0]
        max_length = int(seq.get_shape()[1])
        input_size = int(seq.get_shape()[2])
        index = tf.range(0, batch_size) * max_length + (length-1)
        flat = tf.reshape(seq, [-1, input_size])
        return tf.gather(flat, index)





