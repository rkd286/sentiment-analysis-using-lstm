import warnings
warnings.filterwarnings('ignore')

import tensorflow as tf
import numpy as np
import os
import sys
import datetime
import time
from rnn import RNN
import data_preprocessing

#Flags

#Data Loading
tf.flags.DEFINE_string("file_path", "input/", "File path of data")
tf.flags.DEFINE_float("dev_set_percentage", 0.1, "Percentage of sample as Dev Set")
tf.flags.DEFINE_integer("max_sentence_length", 80, "Max sentence length in data")

#HyperParams
tf.flags.DEFINE_string("cell_type", "lstm", "Type of rnn cell. Choose 'vanilla' or 'lstm' or 'gru' (Default: vanilla)")
tf.flags.DEFINE_string("word2vec", "input/GoogleNews-vectors-negative300.bin", "Word2vec file with pre-trained embeddings")
tf.flags.DEFINE_integer("embedding_dim", 300, "Dimensionality of character embedding (Default: 300)")
tf.flags.DEFINE_integer("hidden_size", 128, "Dimensionality of character embedding (Default: 128)")
tf.flags.DEFINE_float("dropout_keep_prob", 0.5, "Dropout keep probability (Default: 0.5)")
tf.flags.DEFINE_float("l2_reg_lambda", 1.0, "L2 regularization lambda (Default: 3.0)")

#Training parameters
tf.flags.DEFINE_integer("batch_size", 128, "Batch Size (Default: 64)")
tf.flags.DEFINE_integer("n_epochs", 100, "Number of training epochs (Default: 100)")
tf.flags.DEFINE_integer("display_every", 10, "Number of iterations to display training info.")
tf.flags.DEFINE_integer("evaluate_every", 100, "Evaluate model on dev set after this many steps")
tf.flags.DEFINE_integer("checkpoint_every", 100, "Save model after this many steps")
tf.flags.DEFINE_integer("n_checkpoints", 5, "Number of checkpoints to store")
tf.flags.DEFINE_float("learning_rate", 1e-5, "Which learning rate to start with. (Default: 1e-3)")

#Misc Parameters
tf.flags.DEFINE_boolean("allow_soft_placement", True, "Allow device soft device placement")
tf.flags.DEFINE_boolean("log_device_placement", False, "Log placement of ops on devices")

FLAGS = tf.flags.FLAGS
FLAGS(sys.argv)
print('-'*50, "Parameters", '-'*50)
for attribute, value in sorted(FLAGS.__flags.items()):
    print("{} -> {}".format(attribute.upper(), value))
print('-'*100)

def train():
    with tf.device('/gpu:0'):
        X_text, y = data_preprocessing.load_data(FLAGS.file_path)

    # vocab_size = len(y)

    text_vocab_processor = tf.contrib.learn.preprocessing.VocabularyProcessor(FLAGS.max_sentence_length)
    X = np.array(list(text_vocab_processor.fit_transform(X_text)))

    vocab_size = len(text_vocab_processor.vocabulary_)
    print("Text Vocab Size: {}".format(vocab_size))

    #Shuffle Data
    shuffle_indices = np.random.permutation(np.arange(len(y)))
    X_shuffled = X[shuffle_indices]
    y_shuffled = y[shuffle_indices]

    dev_set_index = -1 * int(FLAGS.dev_set_percentage * vocab_size)

    X_train, y_train = X_shuffled[:dev_set_index], y_shuffled[:dev_set_index]
    X_dev, y_dev = X_shuffled[dev_set_index:], y_shuffled[dev_set_index:]

    train_len = len(y_train)
    dev_len = len(y_dev)

    print("Train Test Split Percentage: {}".format((train_len/dev_len)*100))

    with tf.Graph().as_default():
        sess = tf.Session()
        with sess.as_default():

            rnn = RNN(
                sequence_length=X_train.shape[1],
                n_classes=y_train.shape[1],
                vocab_size=vocab_size,
                embedding_size=FLAGS.embedding_dim,
                cell_type=FLAGS.cell_type,
                n_neurons=FLAGS.hidden_size,
                l2_reg_lambda=FLAGS.l2_reg_lambda,
                trainable=True
            )

            global_step = tf.Variable(0, name='global_step', trainable=False)
            optimizer = tf.train.AdamOptimizer(FLAGS.learning_rate)
            train = optimizer.minimize(rnn.loss, global_step=global_step)

            #Output Directory
            timestamp = str(int(time.time()))
            out_dir = os.path.abspath(os.path.join(os.curdir, "runs", timestamp))
            print("Writing output to {}".format(out_dir))

            #Summary
            loss_summary = tf.summary.scalar("loss", rnn.loss)
            acc_summary = tf.summary.scalar("acc", rnn.accuracy)

            #Train Summary
            train_summary_op = tf.summary.merge([loss_summary, acc_summary])
            train_summary_dir = os.path.join(out_dir, "summaries", "train")
            train_summary_write = tf.summary.FileWriter(train_summary_dir, sess.graph)

            #Dev Summary
            dev_summary_op = tf.summary.merge([loss_summary, acc_summary])
            dev_summary_dir = os.path.join(out_dir, "summaries", "dev")
            dev_summary_write = tf.summary.FileWriter(train_summary_dir, sess.graph)

            #Checkpoint Directory
            checkpoint_dir = os.path.abspath(os.path.join(out_dir, "checkpoints"))
            checkpoint_prefix = os.path.join(checkpoint_dir, "models")

            if not os.path.exists(checkpoint_dir):
                os.makedirs(checkpoint_dir)
            saver = tf.train.Saver(tf.global_variables(), max_to_keep=FLAGS.n_checkpoints)
            

            #Initialize Global Variables
            init = tf.global_variables_initializer()
            sess.run(init)

            if FLAGS.word2vec:
                initW = np.random.uniform(-0.25, 0.25, size=(vocab_size, FLAGS.embedding_dim))

                print("Loading Word2Vec binary file: {}".format(FLAGS.word2vec))

                with open(FLAGS.word2vec, 'rb') as f:
                    header = f.readline()
                    word2vec_vocab_size, layer1_size = map(int, header.split())
                    binary_len = np.dtype('float32').itemsize * layer1_size

                    for line in range(word2vec_vocab_size):
                        word = []

                        while True:
                            ch = f.read(1).decode('latin-1')

                            if ch == ' ':
                                word = ''.join(word)
                                break
                            
                            if ch != '\n':
                                word.append(ch)
                            
                        idx = text_vocab_processor.vocabulary_.get(word)
                        if(idx != 0):
                            initW[idx] = np.fromstring(f.read(binary_len), dtype='float32')
                        else:
                            f.read(binary_len)
                        
                    sess.run(rnn.W_text.assign(initW))
                    print("Successfully loaded pre-trained word2vec file.")


                            




            batches = data_preprocessing.batch_iter(list(zip(X_train, y_train)), FLAGS.batch_size, FLAGS.n_epochs)

            for batch in batches:
                X_batch, y_batch = zip(*batch)

                #Train
                feed_dict = {
                    rnn.input_text: X_batch,
                    rnn.input_y: y_batch,
                    rnn.dropout_keep_prob: FLAGS.dropout_keep_prob
                    }
                
                _, step, summaries, loss, accuracy = sess.run([train, global_step, train_summary_op, rnn.loss, rnn.accuracy],
                                                feed_dict=feed_dict)
                train_summary_write.add_summary(summaries, step)
                
                #Training Logs

                if step % FLAGS.display_every == 0:
                    time_str = datetime.datetime.now()
                    print("{}: step: {}\tloss: {}\tacc: {}".format(time_str, step, loss, accuracy))

                
                #Evaluation
                if step % FLAGS.evaluate_every == 0:
                    print("\nEvaluation:")

                    feed_dict_dev = {
                    rnn.input_text: X_dev,
                    rnn.input_y: y_dev,
                    rnn.dropout_keep_prob: 1.0
                    }
                
                    summaries_dev, loss, accuracy = sess.run([dev_summary_op, rnn.loss, rnn.accuracy],
                                                    feed_dict=feed_dict_dev)
                    dev_summary_write.add_summary(summaries_dev, step)
                    
                    time_str = datetime.datetime.now()
                    print("{}: step: {}\tloss: {}\tacc: {}\n".format(time_str, step, loss, accuracy))

                #Model checkpoint
                if step % FLAGS.checkpoint_every == 0:
                    path = saver.save(sess, checkpoint_prefix, global_step=step)
                    print("Saved model checkpoint to {}\n".format(path))


if __name__ == '__main__':
    train()








