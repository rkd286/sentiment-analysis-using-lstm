import numpy as np
import re

def clean_str(string):

    string = re.sub(r"[^A-Za-z0-9(),!?\'\`]", " ", string)
    string = re.sub(r"\'s", " \'s", string)
    string = re.sub(r"\'ve", " \'ve", string)
    string = re.sub(r"n\'t", " n\'t", string)
    string = re.sub(r"\'re", " \'re", string)
    string = re.sub(r"\'d", " \'d", string)
    string = re.sub(r"\'ll", " \'ll", string)
    string = re.sub(r",", " , ", string)
    string = re.sub(r"!", " ! ", string)
    string = re.sub(r"\(", " \( ", string)
    string = re.sub(r"\)", " \) ", string)
    string = re.sub(r"\?", " \? ", string)
    string = re.sub(r"\s{2,}", " ", string)
    return string.strip().lower()


def load_data(file_path):

    positive_data_path = file_path + 'rt-polarity.neg'
    negative_data_path = file_path + 'rt-polarity.pos'

    positive_data = list(open(positive_data_path, 'r').readlines())
    positive_data = [string.strip() for string in positive_data]
    negative_data = list(open(negative_data_path, 'r').readlines())
    negative_data = [string.strip() for string in negative_data]

    # print(positive_data[:5])
    # print(negative_data[:5])

    X = positive_data + negative_data
    X = [clean_str(x) for x in X]

    positive_labels = [[0, 1] for _ in positive_data]
    negative_labels = [[1, 0] for _ in negative_data]
    y = np.concatenate([positive_labels + negative_labels], axis=0)
    

    return [X, y]


def batch_iter(data, batch_size, n_epochs, shuffle=True):

    data = np.array(data)
    data_size = len(data)
    n_batches_per_epoch = (data_size - 1) // batch_size

    for epoch in range(n_epochs):
        print("-"*50, "Epoch: {}".format(epoch), "-"*50)
        if(shuffle):
            shuffle_indices = np.random.permutation(np.arange(data_size))
            shuffled_data = data[shuffle_indices]
        else:
            shuffled_data = data
        
        for batch in range(n_batches_per_epoch):
            start = batch * batch_size
            end = (batch * batch_size) + batch_size
            yield shuffled_data[start:end]


if __name__ == '__main__':
    load_data('./rnn_example_1/input/')
    

